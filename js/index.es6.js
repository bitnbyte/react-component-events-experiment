class InputUI extends React.Component {
  handleKeyUp(evt) {
    var newInput = null
    if (evt.keyCode !== 13) {
      return
    }
    newInput = evt.target.value
    evt.target.value = ''
    document.dispatchEvent(new CustomEvent(
      'uiInputNewItem', {
        detail: { newItem: newInput }
      }
    ))
  }
  render() {
    return <input onKeyUp={ this.handleKeyUp }/>
  }
}

class ListUI extends React.Component {
  constructor() {
    super()
    this.state = {
      items: []
    }
    document.addEventListener(
      'uiInputNewItem', this.handleNewItem.bind(this)
    )
    document.addEventListener(
      'reqListNewItem', this.sendNewItem.bind(this)
    )
  }
  renderItems() {
    return this.state.items.map(this.renderItem)
  }
  removeItem( e ) {
    var index = e.target.getAttribute( 'data-index')
    this.state.items.splice( index, 1 )
    this.setState( { items: this.state.items } )
  }
  renderItem(item, index) {
    return (
      <li>
        <span>{ index + 1 }: { item } </span> 
        <button data-index={ index } 
          onClick={ this.removeItem.bind( this ) }>
           x </button></li>
    )
  }
  render() {
    return (
      <ul>
        { this.state.items.map( this.renderItem.bind( this ) ) }
      </ul>
    )
  }
  handleNewItem(evt) {
    this.state.items.unshift(evt.detail.newItem)
    this.setState(this.state)
    document.dispatchEvent(new Event('uiListNewItem'))
  }
  sendNewItem() {
    var detail = {
      newItem: null
    }
    if (this.state.items.length) {
      detail.newItem = this.state.items[0]
    }
    document.dispatchEvent(new CustomEvent(
      'dataListNewItem', { detail: detail }
    ))
  }
}

class LatestItemUI extends React.Component {
  constructor() {
    super()
    this.state = {
      latestItem: ''
    }
    document.addEventListener(
      'uiListNewItem', this.requestNewItem.bind(this)
    )
    document.addEventListener(
      'dataListNewItem', this.update.bind(this)
    )
  }
  requestNewItem() {
    document.dispatchEvent(new Event('reqListNewItem'))
  }
  update(evt) {
    this.setState({
      latestItem: evt.detail.newItem
    })
  }
  render() {
    return <div>Latest: { this.state.latestItem }</div>
  }
}

class ListApp extends React.Component {
  render() {
    return (
      <div>
        <InputUI />
        <ListUI />
        <LatestItemUI />
      </div>
    )
  }
}

React.render(
  <ListApp />,
  document.getElementById( 'container' )
)