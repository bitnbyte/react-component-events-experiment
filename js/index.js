// This setup demonstrates using components
// that do not reference each other directly.
// Instead, using events enables lightweight 
// swappable components.

// TODO: Add support for IE11

/**
 * An input component
 * @sends uiInputNewItem { detail: { newItem: <string> } }
 * @class InputUI
 */
var InputUI = React.createClass({
	/**
	 * Accepts input and dispatches 'uiInputNewItem' event 
	 * @param MouseEvent evt 
	 */
	handleKeyUp: function(evt, reactId) {
		var newInput = null
		if (evt.keyCode !== 13) {
			return
		}
		newInput = evt.target.value
		evt.target.value = ''
		document.dispatchEvent(new CustomEvent(
			'uiInputNewItem', {
				detail: { newItem: newInput }
			}
		));
	},
	/**
	 * Renders the InputUI component
	 */
	render: function() {
		return (
			<input onKeyUp={ this.handleKeyUp }/>
		)
	}
})

/**
 * A list component
 * @sends uiListNewItem
 * @sends dataListNewItem { detail: { newItem: <string> }}
 * @receives reqListNewItem
 * @receives uiInputNewItem
 */
var ListUI = React.createClass({
	/**
	 * @return Object
	 */
	getInitialState: function() {
		return {
			items: []
		}
	},
	/**
	 * @return Array list elements
	 */
	renderItems: function() {
		return this.state.items.map(this.renderItem)		
	},
	/**
	 * @return Object list element
	 */
	renderItem: function(item) {
		return (
			<li>{ item }</li>
		)
	},
	/**
	 * Sets up event listeners
	 */
	componentDidMount: function() {
		document.addEventListener(
			'uiInputNewItem', this.handleNewItem)
		document.addEventListener(
			'reqListNewItem', this.sendNewItem)
	},
	/**
	 * Sends 'dataListNewItem' event
	 */
	sendNewItem: function() {
		var detail = { 
			newItem: null 
		};
		if (this.state.items.length) {
			detail.newItem = this.state.items[0]
		}
		document.dispatchEvent(new CustomEvent(
			'dataListNewItem', { detail: detail }   
		));	
	},
	/**
	 * Updates state in response to 'uiInputNewItem' event
	 * and dispatches 'uiListNewItem' event 
	 */
	handleNewItem: function(evt) {
		this.state.items.unshift(evt.detail.newItem)
		this.setState({items: this.state.items})
		document.dispatchEvent(new Event('uiListNewItem'))
	},
	/**
	 * Renders the ListUI component
	 */
	render: function() {
		return (
			<ul>
				{ this.renderItems() }
			</ul>
		)
	}
})

/**
 * A component to display the latest item 
 * from a ListUI component
 * @receives uiListNewItem
 * @receives dataListNewItem { detail: { newItem: <string> } }
 */
var LatestItemUI = React.createClass({
	/**
	 * Sets up LatestItemUI initial state
	 */
	getInitialState: function() {
		return { latestItem: '' }
	},
	/**
	 * Sets up event listeners
	 */
	componentDidMount: function() {
		document.addEventListener(
			'uiListNewItem', this.requestNewItem)
		document.addEventListener(
			'dataListNewItem', this.update)	
	},
	/**
	 * Sends 'reqListNewItem' event
	 */
	requestNewItem: function() {
		document.dispatchEvent(new Event('reqListNewItem'))
	},
	/**
	 * Receives 'dataListNewItem' event and updates
	 * state
	 */
	update: function(evt) {
		this.setState({
			latestItem: evt.detail.newItem
		})
	},
	/**
	 * Renders the LatestItemUI component
	 */
	render: function() {
		return <div>Latest: { this.state.latestItem }</div>
	}
})

/**
 * Renders the UI
 */
function SetupUI() {
	
	var containerTwo,
		containerThree;
	 
	// Render the InputUI. #container should already
	// be in the HTML
	React.render(
		<InputUI />,
		document.getElementById('container')
	)
	
	// Create a second container and render 
	// the LatestItemUI
	containerTwo = document.createElement('div')
	containerTwo.id = 'container-two'
	
	if (document.body.appendChild(containerTwo)) {
		React.render(
			<LatestItemUI />,
			containerTwo
		)	
	}
	
	// Create a third container and render
	// the ListUI
	containerThree = document.createElement('div')
	containerThree.id = 'container-three'
	
	if (document.body.appendChild(containerThree)) {
		React.render(
			<ListUI />,
			containerThree
		)	
	}

	// Remove this function from the global space
	SetupUI = void(0)
}

// Initiating setup with in-browser JSX within
// a function expression ( i.e. (function() {})() )
// results in an error. Use a self deleting declared
// function instead.
SetupUI()